import React from 'react';
import SearchBar from './SearchBar';
import unsplash from '../apis/unsplash';
import ImageList from './ImageList';

const styles = {
    marginTop: '10px'
}

class App extends React.Component {
    state = {
        images: []
    }
    onSearchSubmit = async (term) => {
        const response = await unsplash.get('/search/photos', {
            params: {
                query: term,
            }
        });

        this.setState({
            images: response.data.results
        })
    }

    render() {

        console.log(this.state.images);
        return (
            <div className="ui container" style={styles}>
                <SearchBar onSubmit={this.onSearchSubmit}/>
                <ImageList images={this.state.images} />
            </div>
        );
    }
}

export default App;
