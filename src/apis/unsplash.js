import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers: {
        Authorization: 'Client-ID 5D8_T19WuIcXFfVqWplL2Pcm3CfaRiaFzJUprdkjmHQ'
    }
});
